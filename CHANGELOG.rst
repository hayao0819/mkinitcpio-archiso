#########
Changelog
#########

[59] - 2021-11-30
=================

Added
-----

- Add a mailmap file for better author integration in git

Changed
-------

- Make PGP verification more verbose and fix message output

[58] - 2021-07-31
=================

Added
-----

- Add README.rst and CONTRIBUTING.rst
- Add editorconfig integration
- Add GitLab CI integration

Changed
-------

- Adapt Makefile to new repository layout and only test and install files that are part of mkinitcpio-archiso
- Fix formatting of hooks and scripts according to shfmt
